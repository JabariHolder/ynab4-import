import angular from 'angular';

let cloudComputing = () => {
  return {
    template: require('./cloudComputing.html'),
    restrict: 'E'
  }
};

angular.module('cloudComputing', [])
  .directive('cloudComputing', cloudComputing);